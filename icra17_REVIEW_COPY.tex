\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out if you need a4paper

%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4 paper
\usepackage[linesnumbered,ruled]{algorithm2e}
\usepackage{color}
\usepackage{listings}
\usepackage{pgfplots} 
\usepackage{pgfplots} 
\usepackage{listings}
\usepackage{gensymb}
\usepackage{xcolor}
\IEEEoverridecommandlockouts                              % This command is only needed if 
\newcommand\todo[1]{{\emph{\textbf{\color{red}TODO: #1}} }}
\long\def\/*#1*/{}
\newcommand\addref[1]{{\emph{\textbf{\color{blue}ADDREF: #1}} }}
\long\def\/*#1*/{}
                                                        % you want to use the \thanks command

\overrideIEEEmargins                                      % Needed to meet printer requirements. 

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed



\newcommand{\argmax}[1]{\underset{#1}{\operatorname{arg}\,\operatorname{max}}\;}
\newcommand{\Alg}[1]{Algorithm~\ref{alg:#1}}
\newcommand{\Line}[1]{Line~\ref{line:#1}}
\newcommand{\Lines}[2]{Lines~\ref{line:#1}-\ref{line:#2}}


\title{\LARGE \bf Semantic Web-Mining and Deep Vision for Lifelong Object Discovery 
}


\author{Jay Young$^{1}$, Lars Kunze$^{1}$, Valerio Basile$^{2}$, Elena Cabrio$^{2}$, Nick Hawes$^{1}$ and Barbara Caputo $^{3}$
\thanks{$^{1}$Intelligent Robotics Lab, University of Birmingham, United Kingdom
        {\tt\small \{j.young,kunzel,n.a.hawes\}@cs.bham.ac.uk}}%
\thanks{$^{2}$Institut national de recherche en informatique et en automatique, WIMMICS, France
        {\tt\small \{valerio.basile,elena.cabrio\}@inria.fr}}
\thanks{$^{3}$University de Roma, Sapienza, Italy
        {\tt\small caputo@dis.uniroma1.it}}%
}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
 Autonomous robots that are to assist humans in their daily lives must recognize and understand the meaning of objects in their environment. However, the open nature of the world means robots must be able to learn and extend their knowledge about previously unknown objects on-line. In this work we investigate the problem of unknown object hypotheses generation, and employ a semantic web-mining framework along with deep-learning-based object detectors. This allows us to make use of both visual and semantic features in combined hypotheses generation. Experiments on data from mobile robots in real world application deployments show that this combination improves performance over the use of either method in isolation. 
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Mobile service robots deployed in human environments such as offices, homes, industrial workplaces and similar locations must be equipped with ways of representing, reasoning and learning about the objects in their environment. Equipping a robot \emph{a priori} with a (necessarily closed) database of object knowledge is difficult, because the system designer must predict which subset of all possible objects is required, and then build these models (a time-consuming task). If a new object appears in the environment, or an unmodelled object becomes important to a task, the robot will be unable to perceive, or reason about it. A solution to this problem is to give robots the ability to extend their own knowledge-bases on-line using information about new objects they encounter, and the capability to build models based on their own situated experiences. But it is not enough for a robot to merely learn a perceptual model of a newly observed cluster of 3D points or 2D pixels. Some form of \textit{semantic} information is desirable too -- for instance, how does it \textit{relate} to other objects in the environment, where it might be \textit{found}, what it is \textit{used for} and where \textit{should it go}. 
We refer to the linking of semantic knowledge to a previously unknown visual object as \emph{hypothesis generation}.

While perceptual information about objects can be learned directly by a robot platform from its own situated observations~\cite{Faeulhammer:2016}, the question of how these observations are linked to semantic information is less clear. We expect that structured and semi-structured Web sources such as Wikipedia, DBPedia and WordNet~\cite{kilgarriff2000wordnet} can be used to answer some of these questions. 

A data source of particular interest to us is ImageNet, which is a large, ever-evolving database of categorised images organised using the WordNet lexical ontology. The ImageNet Large Scale Visual Recognition Challenge (ILSVRC) \cite{ILSVRC15} has in recent years produced machine learning tools trained on ImageNet for object detection and image classification. Of particular interest to us are \textit{deep learning} based approaches using Convolutional Neural Networks, trained on potentially thousands of categories \cite{NIPS2012_4824}. 
We expect such large-scale object detectors to be valuable in our hypotheses generation task as they provide a potential bridge between a robot's situated experience of objects and their associated semantic information. However, such an approach raises the question of how well such predictors perform when queried with the challenging image data endemic to mobile robot platforms, as opposed to the cleaner, and higher-resolution, data they are typically evaluated on. Also, while this does not entirely address the problem of which objects to model in advance -- using a CNN trained on ImageNet is still using a pre-trained detector, just one with a very large training set -- the potential benefits are large, and would still allow us to extend a robot's knowledge base far beyond what it can be manually equipped with in advance of a deployment. Further, ImageNet is always growing and improving, so a robot's knowledge base could grow as new objects are added to it.

In this paper we investigate how semantic web-mining and deep vision can be combined to generate semantic label hypotheses for objects detected in real environments. These label hypotheses are linked to \textit{structured, semantic knowledge bases} such as DBPedia and WordNet, allowing us to link perceptual experience with higher-level knowledge. This paper makes the following contributions:
\begin{itemize}
	\item A novel approach for predicting the semantic identity of unknown, everyday objects based on web-mining using distributional semantics and Deep Vision.
	\item A surface-based approach to object learning on mobile robot platforms.
	\item An evaluation of our technique on real-world robot perception data from two long-term deployments.
	\item Provision of the software tools used to produce this work as open source software.
\end{itemize}

% We also make the object hypothesis prediction framework, surface-based object segmentation algorithm and the offline ground-truth object labelling tool used in this work available as free software for the community to use as desired.
 
\section{Related Work}

To obtain information about unknown objects from the Web, a robot can use
perceptual and/or knowledge-based
queries. %Future systems will inevitably need to use both.
In this paper we use both types of queries. Knowledge-based queries can be seen
as complementary to image-based queries which search databases of labelled
images for similarity, e.g.~\cite{Philbin:2008}, or use web services such as
Google Goggles to extract text, logo, and texture information
\cite{robosherlock, icra14ensmln}.

Although the online learning of new \emph{visual} object models is currently a niche area in robotics, some approaches do exist~\cite{Faeulhammer:2016,finman2013toward}. These approaches are capable of segmenting previously unknown objects in a scene and building models to support their future re-recognition. However, this work focuses purely on apperance models, and does not address how the learnt objects are described semantically. In a more supervised setting, many approaches have used humans to train mobile robots about new objects in their environment~\cite{GemignaniRAS15} and robots have also used Web knowledge sources to improve their performance in closed worlds, e.g. the use of object-room co-occurrence data for room categorisation in~\cite{hanheideetal2011dora}. 

Our predictions for unknown objects rely on determining the semantic relatedness of terms. This is an important topic in several areas, including data mining, information retrieval and web recommendation. \cite{Schuster:2012} applies ontology-based similarity measures in the robotics domain. Background  knowledge  about  all  the  objects  the robot could encounter, is stored in an extended version of the \textsc{KnowRob} ontology \cite{tenorth13knowrob}. Then, WUP similarity \cite{Wu:1994:VSL:981732.981751} is applied to calculate relatedness of the concept types by considering the depth of the concepts and the depth of their lowest common super-concept in the ontology. 
\cite{leal_et_al:OASIcs:2012:3519} presents an approach for computing the semantic relatedness of terms using ontological information extracted from DBpedia for a given domain, using the results for music recommendations.
We compute the semantic relatedness between objects in mined text by leveraging the vectorial representation of the DBpedia concepts provided by the NASARI resource~\cite{Camacho-Collados15}. This method links back to earlier distributional semantics work (e.g. Latent Semantic Analysis~\cite{Landauer97asolution}) with the difference that here concepts are represented as vectors, rather than words.


\section{Task Description and Data Collection}

In our work we consider a mobile service robot -- in our case a MetraLabs Scitos G5, equipped with an ASUS Xtion RGB-D camera -- tasked with observing everyday scenes in an unprepared human environment. By unprepared we mean that these are organically occurring scenes which we have not altered the scenes in anyway. We do not use the term ``unstructured'' because often these scenes have a natural structure that we wish to  exploit. 

Our robot is provided a map of the deployment environment, and each day it generates tasks to observe pre-selected cabinet tops, kitchen counters and other surfaces we determined to be potentially interesting for an object-learning robot. In the deployment environment we selected $~30$ surfaces of potential interest. Given a surface to observe, the robot takes multiple views from various angles, currently limited to $3$. Our views are chosen using our ViPER library\footnote{https://github.com/kunzel/viper} which generates candidate views of surfaces in a stochastic way, and selects a limited set that aims to maximise coverage of the area. On each view, we segment the scene using a standard RANSAC-based plane-pop-out segmentation algorithm to remove table-like planes from the 3D point cloud, and cluster the results using the DBSCAN clustering algorithm. We found this approach to be a good trade-off between speed, accuracy and ease of implementation, though much attention needed to be paid to noise filtering to ensure good results. The two most successful filters we found were a simple image size filter to ensure the robot did not present object candidates that were too small or too big (and likely to be sensor artefacts), and a luminance filter that rejected object candidates that were too dark or too light. In Figure \ref{fig:mugs} we show some examples of mugs that were discovered by our robot.


\begin{figure}
\center
\includegraphics[scale=0.6]{mugs.jpg}
\caption{A selection of mugs encountered by the robot.}
\label{fig:mugs}
\end{figure}

After taking multiple views of a surface, we achieve coherence between any objects that have been segmented by simply measuring the overlap between clusters. Between two discrete views taken at $T_0$ and $T_1$, each segment at $T_1$ reports the proportion of its points which overlap the bounding boxes of the segments observed at $T_0$. We then link segments based on their highest proportion of shared points, and allows us to represent multiple views of the same object. This is accomplished entirely using 3D positional information, and does not consider the RGB values of points. This allows us to link multiple views of a single object, which are then aligned into the same co-ordinate space using the algorithms provided by the meta-room toolkit \cite{ambrucs2014meta}.

\section{Semantic Object Knowledge Representation}
Object models learned by the robot are stored in a long-term memory using our separately developed SOMa software\footnote{http://github.com/strands-project/soma}, a database intended for representing and annotating objects in a robot's world model, augmented with a queryable spatio-temporal  store. 

In this paper we assume the robot is tasked with observing objects in unprepared, human environments. Whilst this is not a service robot task in itself, it is a precursor to many other task-driven capabilities such as object search, manipulation, human-robot interaction etc. Similar to prior work (e.g.~\cite{Schuster:2012}) we assume that the robot already has a semantic map of its environment which provides it with at least 3D models of supporting surfaces (desks, worktops, shelves etc.), plus the semantic category of the area in which the surface is located (office, kitchen, meeting room etc.). Surfaces and locations are linked to DBpedia, typically as entities under the categories \texttt{Furniture} and \texttt{Room} respectively. 



\subsection{Static and Dynamic Object Context}
We split the spatial-semantic context representation of an object into a static part and a dynamic part. For the static part, we annotated local landmarks on our map of the environment, restricted to large non-moving objects such as coffee machines, photocopiers, sinks, fax machines, whiteboards, and other objects that are typically stationary on a day-to-day basis. Also part of the static representation is a label such as \texttt{Kitchen} or \texttt{Office} that broadly describes the usage of a particular area. For the dynamic part, we included any object detected from a pre-trained set taken from the deployment environment and learned manually using the V4R toolkit\footnote{https://github.com/strands-project/v4r}, which also provides our recognition software.


\section{Semantic Web-Mining}
In previous work we developed a Semantic Web-Mining component for robot systems~\cite{young2016towards}. This component provides access to object- and scene-relevant knowledge extracted from Web sources, and is accessed using JSON-based HTTP requests. The structure of a request to the system describes the objects that were
observed with an unknown object, the spatial relations held
between each object, calculated exhaustively pairwise, as well as the room and surface labels describing where in the environment the observations were made. Upon receiving a query, the service computes the \textit{semantic relatedness} between each object included in the co-occurrence structure and every object in a large set of candidate objects from which possible concepts are drawn from. This semantic relatedness is computed by leveraging the vectorial
representation of the DBpedia concepts provided by the NASARI
resource~\cite{Camacho-Collados15}. The NASARI resource represents BabelNet concepts~\cite{Navigli2012217} as a vector in a high-dimensional geometric
space. The vector components are computed with the \textit{word2vec}~\cite{mikolov2013efficient} tool, based on the co-occurrence of mentions of each concept, in this case using Wikipedia as source corpus.
Using the \textit{distributional hypothesis}, vectors that
represent related entities end up close in the vector space, allowing us to measure relatedness by computing the inverse of the
cosine distance between two vectors. For instance, the NASARI vectors
for \texttt{Pointing\_device} and \texttt{Mouse\_(computing)} have
relatedness $0.98$ (on a continuous scale from $0$ to $1$),
while \texttt{Mousepad} and \texttt{Teabox} are $0.26$ related. The system computes the aggregate of the relatedness of a candidate object to each of the scene objects contained in the query. Formally, given $n$ observed objects in the query $q_1, ..., q_n$, and
$m$ candidate objects in the universe under consideration $o_1, ...,
o_m \in O$, each $o_i$ is given a score that indicates its likelihood of
being the unknown object by aggregating its relatedness across all observed objects. The aggregation function we use to give the likelihood of an object $o_i$ is given by:

$$
likelihood(o_i) = \prod_{j=1}^{n} relatedness(o_i, q_j)
$$


We also make use of \textit{Qualitative Spatial Relations} (QSRs) to represent information about objects~\cite{Frommberger2010a}. QSRs discretise continuous spatial measurements, particularly relational information such as the distance and orientation between points, yielding symbolic representations of ranges of possible continuous values. In this work, we make use of a qualitative distance measure, often called a Ring calculus. When observing an object, we categorise its distance relationship with any other objects in a scene with the following set of symbols: ${near_0,near_1,near_2}$, where $near_0$ is the closest. This is accomplished by placing sets of thresholds on the distance function between objects, taken from the centroid of the 3D cluster. 
Since this information is represented as a discrete level or proximity
(from \texttt{near\_0} to \texttt{near\_3}), we can use this as a
threshold to determine whether or not an object should be included in a 
relatedness calculation. In this work we discard any object related
by \texttt{near\_3}, based on the intuition that the further away an
object is spatially, the less related it is. In this way, the spatial proximity of observed objects is taken into account in the search for semantically related objects. For more details see \cite{young2016towards}.


\section{Deep Vision}
Machine Learning algorithms are highly sensitive to data representation, and varying representations can cause variability in the sorts of features and knowledge available to a learning mechanism. Deep learning algorithms \textit{learn} hierarchical forms of data representation, and have in recent years  become extremely popular in the fields of language processing and computer vision, in tasks such as image classification, object detection and semantic segmentation. Deep learning in general is being explored more and more by the robotics community, being used for such tasks as grasp detection \cite{lenz2015deep} and visual perception tasks \cite{giusti2016machine}. 

In this work, we make use of Deep Convolutional Neural Networks trained on data from the ImageNet project of crowd-sourced annotated images. Specifically we make use of the CNN architecture of Krizhevsky et. al \cite{NIPS2012_4824}, which is implemented in the Caffe toolkit \cite{jia2014caffe}. This is attractive to us, as such predictors are trained on an extensive amount of data, but such models are relatively small and computationally cheap to query. In our system we use this as a predictor, and pass in cropped images of objects the robot discovers autonomously. In return, the CNN provides a ranked list of object label hypotheses as WordNet classes.

\section{Combining Semantic Web-Mining and Deep Vision}
We seek to combine the predictive power of our Semantic Web-mining approach and existing Deep Vision techniques into a single system capable of generating object label hypotheses. We also wish this to be possible in a \textit{multi-view} way, where multiple observations of a single object are made from different vantage points. In such cases the quality of individual views may not be consistent with one-another due to environmental noise, occlusion, or other dynamics that mean one or more views may be of higher quality than others.

Our approach to combining our context-based and the vision-based prediction systems is to use the context system to filter the predictions from the CNNs down to those expected to be most salient, given the spatial context of the unknown object. 

\Alg{labelpred} describes our approach in pseudo code. The algorithm takes the
following arguments as its input (\Line{header}): a cropped image of the target
object $I$; two lists of labels which determine the dynamic ($C_D$) and the
static ($C_S$) context in the scene; and two parameters $n$ and $t$ which
control the filtering process (here we used n=7 and t=.8). The algorithm
returns a label for the target object $l^\ast$. First, we initialize the set of
candidate labels (\Line{init}). We then predict a set of labels and their
corresponding confidences from the cropped image $I$ using a trained CNN
(\Line{cnn}). After selecting the $n$ best labels from the CNN (\Line{nbest}),
we iterate over these labels, and relate them to all context labels by
computing the WUP score (\Line{wup}). If the WUP score is larger than the
predefined threshold $t$ (\Line{threshold}), we add the CNN label to the list
of candidates (\Line{add}). Eventually we select the label with the highest
confidence from all candidates (\Line{argmax}) and return it (\Line{return}).

In the multi-view case, we perform this process for every view, and use a
voting system whereby the most confident prediction for a single view counts as
one vote, and the most common label across all views of an object is chosen as
the predicted label for a given observation episode.

\begin{algorithm}[tb!]
  \DontPrintSemicolon
  \SetSideCommentLeft 
  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}
  \underline{Function predictObjectLabel} $(I, C_{D}, C_{S}, n, t)$\; \label{line:header}
  \Input{Cropped image $I$; Dynamic context $C_{D}$; Static context $C_{S}$; Number of CNN and context candidates $n$; Semantic relatedness threshold $t$}
  \Output{Label $l^\ast$}
  \Begin{
    $L_{Candidates} \leftarrow \emptyset$\; \label{line:init} 
    $L_{CNN} \leftarrow  predictLabelsFromCNN(I)$\; \label{line:cnn}
    $L'_{CNN} \leftarrow selectNBestLabels(L_{CNN},n)$\; \label{line:nbest}
    \For{$l \in L'_{CNN}$}{ \label{line:fornbest}
      \For{$c \in (C_{D} \cap C_{S})$}{ \label{line:forcontext}
        $wup \leftarrow computeWUP(l,c)$\; \label{line:wup}
        \If{$wup > t$}{ \label{line:threshold}
          $L_{Candidates} \leftarrow L_{Candidates} \cap{}\ l$\; \label{line:add} 
        }
      }
    }
    $l^\ast \leftarrow \argmax{l} L_{Candidates}$\; \label{line:argmax}
    \Return{$l^\ast$}\label{line:return}
  }
  \caption{Object Label Prediction based on Semantic Web-Mining and Deep Vision}
  \label{alg:labelpred}
\end{algorithm}

Our approach attempts to prefer vision results for which there may be weaker visual features, but for which we can say there are strong \textit{contextual} indicators as to the category of the object. We regard this as a \textit{soft} filter, meaning that we do not restrict the final prediction to strictly only the things the context system can predict, but we prefer things that are strongly related to the things it \textit{does} predict. Overall, our approach can be seen as a heuristic for selecting which label to pick from the set of CNN results.


\begin{figure}
\center
\includegraphics[scale=0.08]{bob_kitchen.jpg}
\caption{Our robot observing a kitchen scene.}
\label{fig:bob_microwave}
\end{figure}

As a worked example, in one occasion the robot observes an object for which the ground truth is Microwave, similar to Figure \ref{fig:bob_microwave}. Our CNNs provide us a ranked list of potential labels, the top ranked of which being \texttt{Safe} (as in a safety deposit box), the second being \texttt{Crate}, with confidences $0.47$ and $0.34$ respectively, the third being \texttt{Microwave} with a confidence of $0.27$, and the final two predictions being \texttt{Fire screen} ($0.12$) and \texttt{Screen} ($0.013$). Our context system predicts various kitchen appliances and objects, given that the object is seen in a room labelled Kitchen, and there are objects like a kettle, a mug and a fridge nearby. We find that in the context predictions suggestions such as \texttt{Oven} and \texttt{Toaster} relate strongly to the \texttt{Microwave} prediction of the CNN (WUP $0.92$ in both cases), where the \texttt{Crate} and \texttt{Safe} predictions do not relate strongly to any of the context predictions (WUP $0.52$ and $0.50$ respectively), nor do the \texttt{Fire screen} or \texttt{Screen} predictions (again $0.52$ and $0.50$ respectively, indicating these entries are distant). As such, filtering means we drop all hypotheses from the CNN results except for Microwave, which we then put forward as the label for this view -- if we had multiple possible candidates after filtering, we would defer to their original CNN ranking and pick the highest. In the case of there being no candidate that is above the relatedness threshold, we will always defer to the vision system, and we see this occurs in $23.40\%$ of cases in dataset B and $37.9\%$ of cases in dataset A. Upon taking multiple views however, we find that occasionally the label \texttt{Dishwasher} wins out instead, being suggested by the CNN with a high confidence and \textit{also} being highly related to our context predictions. Overall however this error is lessened by the combination of votes from multiple views -- more views vote for the \texttt{Microwave} label than the \texttt{Refrigerator} label overall -- meaning that our prediction of the object's label is overall correct in this particular encounter.

\section{Experiments}
We evaluate our system on two datasets, both collected by the same robot platform in two separate, large workplace environments across two deployment episodes. Dataset A was collected using our surface-based object learning system described previously in this paper, and dataset B was collected using the meta-room paradigm of \cite{ambrucs2014meta}. The core difference between these modalities is that our approach is based on \textit{directed} views onto \textit{specific} surfaces, whereas the meta-room approach is largely a brute-force approach, making 360\degree  scans of areas in the deployment environment. Both datasets were labelled by hand, and along with this paper we make our labelling tool available to the community as Open Source software \footnote{http://github.com/jayyoung/lwann}. Dataset B contains around $~2400$ views of individual objects. For each object instance, there are in general around 2-5 views, of the 18 objects in the experiment on average this gives us between 20-25 instances. Since the deployment environment was an active, working office, some objects -- such as monitors -- are overrepresented in the data, due to there being many in the environment. The least common objects were the Fire Extinguisher and the Microwave, as the robot did not as often visit the small kitchen in which they resided.

In our experiments, our measure of success is the WUP similarity \cite{Wu:1994:VSL:981732.981751} between the prediction of a system and the ground truth object label. To do so we map our DBPedia-based labels from the context prediction system to their equivalent WordNet classes, as this is the domain used by ImageNet. Certainly we could have used the single vectorial representation throughout -- rather than just for the measurement of co-occurence in the web-mining component, as we do. We chose instead to use WordNet concept distance when calculating the accuracy of predictions against ground truth. We found this measure to be less noisy than a DBPedia distance in constraining the meaning of objects, and operates on more abstract entity types than DBPedia (Bluetooth Keyboard, Cordless Keyboard, Ergonomic Keyboard in DBPedia VS. Just \textit{Keyboard} in WordNet). Objects in our system maintain links to \textit{both} representations, allowing us to make use of information found in one resource but not the other, and vice-versa, in the future. WUP similarity is one standard measure of calculating the semantic relatedness of word senses in the lexical ontology of WordNet. A WUP score of $1.0$ means two concepts are identical. For instance, in WordNet the concepts \textit{dog} and \textit{cat} have a WUP score of $0.86$, a \textit{computer keyboard} and a \textit{mouse} have WUP score of $0.80$, a \textit{laptop computer} and a \textit{cow} have a WUP score of $0.30$. We regard any WUP score above $0.70$ as indicating a good categorical relation.

Our use of the WUP score, as opposed to a binary true/false accuracy measure, is because we are interested in predictions that are \textit{strongly categorically related} to the true identity of unknown objects. We do not view the system described in this paper in isolation, and consider it as an important component in an overall, integrated approach to unknown object identification for mobile robots. In our next steps, a reasonable list of hypotheses will allow us to boost our ability to employ more specific, and  potentially expensive, methods, for selecting from a set of hypotheses towards extending its knowledge-base. For instance, the robot may present the objects it has discovered in the environment to its human co-inhabitants and ask for their help in refining its hypotheses. But in order to do so intelligently and efficiently, a set of \textit{initial} hypotheses is crucial.

For experiments with dataset B we employed a leave-one-out approach, where the context for a given object is provided as the other objects in the scene. This provides parity with previous experiments \cite{young2016towards}. In experiments with dataset A, we use the static context and results of pre-trained object recognisers described previously.


\section{Results}

\begin{figure}
\input{g4s_wup.tex}
\caption{WUP Values between Ground Truth and Predicted Object Label for DataSet B, gathered using the meta-room approach.}
\label{fig:g4s_results}
\end{figure}

\begin{figure}
\input{tsc_wup.tex}
\caption{WUP Values between Ground Truth and Predicted Object Label for DataSet A -- gathered using our surface-based approach with view planning.}
\label{fig:tsc_results}
\end{figure}


The results of our experiments are shown in Figures \ref{fig:g4s_results} and \ref{fig:tsc_results}. In addition we also performed t-tests with our combined system against context and vision results individually, which reported $p < 0.01$ in all cases. In all cases each system uses the view-voting approach described previously, with the label for a particular set of views of a single object being the most common top-ranked label amongst all views. 


In general our results show that our system is able to effectively constrain label hypotheses, and in several cases is either entirely accurate (as with the Drinking Glass and Cup seen in dataset A), or comes extremely close (As with the Microwave, Keyboard and Monitor in dataset B). However we observe that accuracy does go down the more noisy the views -- in the case of Dataset A, the robot only encountered 3  mugs during its time in the environment, taking a total of 11 views. On the other hand, there are over 160 views across 30 instances of various types of cups in Dataset B. In general we find that a major influence on performance is object coverage -- the proportion of pixels in a view belonging to a single object. The closer the robot is to an object, the larger it is in the camera frame, and the better results we get from the CNN. Larger objects, such as Keyboards, Laptops and Monitors, and the Microwave in Dataset B, are typically well-covered.

The results highlight multiple interesting problems with our approach. The first of which being that, since we had no control over the classes the CNN was trained with, we could not guarantee that the specific objects the robot observed in the environment would be detectable. In dataset B we see that the \textit{Fire Extinguisher} is the object that all systems have the most trouble with -- the reason why the CNN performs so poorly is that it does not appear to have been trained with any images similar to the the kind of Fire Extinguisher the robot observed in the environment. The reason why the context-based system fails is because the Fire Extinguisher was observed in a kitchen, amongst other typical kitchen utensils and objects, and a strong relationship was unable to be found between the objects that could be observed and a Fire Extinguisher. This highlights a flaw in our context-based approach in that it struggles to identify objects that are \textit{surprising} or out of place -- ideally in such a case we would hope for the vision system to rectify this since, as discussed previously, if no label passes our relatedness test we fall back to the predictions of the CNN entirely. But here, both systems fail. The same is true for the Banana in Dataset A -- it is observed in a workspace environment amongst various pieces of computer hardware, and so the context system constraints its predictions to those kinds of objects. The inability to cope properly with objects that are out-of-context with their environment is a significant limitation of our system. What is particularly frustrating is that the CNN \textit{does} typically recognise bananas when they are observed, and reports a confidence of around $~0.70$, which is excellent and can be put down to the distinct shape and texture of the fruit. It would seem that the right thing to do would be to allow exceptions to the context filter in the event of the CNN being extremely confident about the identity of an object, however not all instances of this problem will be as easily solved as the example of the banana, and this is an area for future study. 

\section{Discussion}
We evaluated our system on real-world unprepared scenes, which were necessarily noisy, diverse, and featured dynamics such as occlusion and varied lighting conditions. We know from our own experimentation that the CNNs we used perform very well on clear, high-resolution images of objects similar to those observed by our robot. But their performance on robot data is significantly worse, and we believe this highlights the limitations of several of our approaches. Certainly typical arguments can be made that the performance of such vision systems can be improved by using more expensive, higher-resolution sensors, and we do agree that easy gains can be made in this way. But in the long-run this does not contribute to our underlying, fundamental understanding -- a \textit{human} is able to identify objects from the kind of noisy, low-resolution data our robot has collected, and so should a robot platform. We believe that \textit{robot vision} must be treated as its own distinct area of research, where the problems of perception and action must be addressed in an integrated way, taking into account the specific dynamics and problems associated with mobile platforms. The best way to work towards that goal is to develop and evaluate robot-centric algorithms and techniques, and evaluate them in situated, real-world scenarios, rather than scenarios where the designer has influenced the experimental set-up and may unconsciously introduce bias or reduce noise and dynamics.

One particular issue our work has brought to light is the difficulty of knowing where to look in order to learn objects. In our most recent deployment, we annotated upwards of 30 surfaces for our robot to visit and attempt to learn from. However, the robot can only observe one surface at a time, and only performs the task a handful of times per day, and so to gain the most information from the task it should be equipped, or be able to learn, some knowledge about where objects might be found. We often saw the robot investigating surfaces that were empty, or were not being used at that time of day, or that were never used at all. Without prior knowledge of the environment and the habits of the humans that inhabit it, we can only make educated guesses as to where interesting objects might be found. We are currently investigating how a robot might learn this information over time, and gradually avoid areas it knows it is unlikely to learn anything new from.

%In general we believe we have only scratched the surface of this and related problems. One problem of particular interest is how the humans that inhabit or work in the same environment as the robot can help in this task. For instance, by confirming, correcting or rejecting object label hypotheses generated by the robot. In such a case, we envision the role of a system such as ours being to help the robot ask the most salient questions about the objects it encounters. How its human colleagues fit into this loop is a major question we have not yet properly addressed, and so this is where our next steps are likely to be.%

\section{Conclusion}

We presented a system that allows a mobile robot to generate label hypotheses for unknown objects it encounters in its environment. We used a semantic web-mining system that allows us to generate candidate labels for an object given it's spatial-semantic context, and coupled this with a deep vision system trained on ImageNet. Using the context predictions as a preference heuristic to select a subset of the predictions made by the deep vision algorithm, we tested this system on data gathered by a robot operating in two real-world workplace environments, and using two different modalities of  data collection. Our results showed that we are able to effectively constrain the set of possible labels for a given object using our approach, though large variance in performance is seen depending on the uniqueness of the object and the availability of trained classifiers. This work highlights the crucial need for integrated approaches to robot perception, and for those techniques to be evaluated on \textit{situated}, real-world data.  

\textit{The research leading to these results has received funding from the European Union Seventh Framework Programme (FP7/2007-2013) under grant agreement No 600623, STRANDS, and under the ALOOF project (CHIST-ERA program).}



\bibliographystyle{IEEEtran}
\bibliography{icra17}



\end{document}
